/**
 * @fileoverview Assigment 3, Script that sinthetize a program using the buttom-up algorithm
 * removing the programs who have lowest probabilty.
 *
 * @version 0.1
 *
 * @author Diego Carrillo Verduzco <dixego@ciencias.unam.mx>
 * @author Fernando Abigail Galicia Mendoza <fernandogamen@ciencias.unam.mx>
 *
 * Course: Sintesis de Programas
 * School cycle: 2019-1
 * Proffesor: Dr. Armando Solar Lezama
 * Program: Maestria en Ciencia e Ingeniera de la Computacion
 * Institute: Instituto de Investigaciones en Matematicas Aplicadas y Sistemas
*/

/*
 ************************************************************************************************************
 ***** Syntax
 ************************************************************************************************************
*/

// Variables to identify the constructors of the language.
var NUM = "NUM";
var FALSE = "FALSE";
var VR = "VAR";
var PLUS = "PLUS";
var TIMES = "TIMES";
var LT = "LT";
var AND = "AND";
var NOT = "NOT";
var ITE = "ITE";

var ALLOPS = [NUM, FALSE, VR, PLUS, TIMES, LT, AND, NOT, ITE];

/**
 * Function that recibes an object, and return their string representation.
 * @param {Object} - Object that will be represented.
 * @return {String} String representation of the object.
*/
function str(obj) { return JSON.stringify(obj); }

/*
 ************************************************************************************************************
 Syntax: Constructors
 ************************************************************************************************************
*/

/**
 * Function that creates the false value.
 * @return{{Exp,String}} Tuple (t,n,s) where t is the false value and s is their string representation.
*/
function flse() {
    return { type: FALSE, toString: function () { return "false"; } };
}

/**
 * Function that creates the variable expression (VR n).
 * @param {String} - Name of the variable.
 * @return{{Exp,String,String}} Tuple (t,n,s) where t is the VR expression, n is their name and s is their string representation.
*/
function vr(name) {
    return { type: VR, name: name, toString: function () { return this.name; } };
}

/**
 * Function that creates the number expression (NUM n).
 * @param {String} - Name of the variable.
 * @return{{Exp,Int,String}} Tuple (t,n,s) where t is the NUM expression, n is their value and s is their string representation.
*/
function num(n) {
    return { type: NUM, val: n, toString: function () { return this.val; } };
}

/**
 * Function that creates the plus expression (PLUS e1 e2).
 * @param {Exp} - First operand of the plus.
 * @param {Exp} - Second operand of the plus.
 * @return{{Exp,Exp,Exp,String}} Tuple (t,x,y,s) where t is the PLUS expression, x is their first operand, y is their second operand and s is their string representation.
*/
function plus(x, y) {
    return { type: PLUS, left: x, right: y, toString: function () { return "("+ this.left.toString() + "+" + this.right.toString()+")"; } };
}

/**
 * Function that creates the product expression (TIMES e1 e2).
 * @param {Exp} - First operand of the product.
 * @param {Exp} - Second operand of the product.
 * @return{{Exp,Exp,Exp,String}} Tuple (t,x,y,s) where t is the TIMES expression, x is their first operand, y is their second operand and s is their string representation.
*/
function times(x, y) {
    return { type: TIMES, left: x, right: y, toString: function () { return "(" + this.left.toString() + "*" + this.right.toString() + ")"; } };
}

/**
 * Function that creates the lower than expression (LT e1 e2).
 * @param {Exp} - First operand of the lower relation.
 * @param {Exp} - Second operand of the lower than relation.
 * @return{{Exp,Exp,Exp,String}} Tuple (t,x,y,s) where t is the LT expression, x is their first operand, y is their second operand and s is their string representation.
*/
function lt(x, y) {
    return { type: LT, left: x, right: y, toString: function () { return "(" + this.left.toString() + "<" + this.right.toString() + ")"; } };
}

/**
 * Function that creates the conjuction expression (AND e1 e2).
 * @param {Exp} - First operand of the conjuction.
 * @param {Exp} - Second operand of the conjuction.
 * @return{{Exp,Exp,Exp,String}} Tuple (t,x,y,s) where t is the AND expression, x is their first operand, y is their second operand and s is their string representation.
*/
function and(x, y) {
    return { type: AND, left: x, right: y, toString: function () { return "(" + this.left.toString() + "&&" + this.right.toString() + ")"; } };
}

/**
 * Function that creates the not expression (NOT e).
 * @param {Exp} - Operand of the not.
 * @return{{Exp,Exp,String}} Tuple (t,x,s) where t is the NOT expression, x is their operand and s is their string representation.
*/
function not(x) {
    return { type: NOT, left: x, toString: function () { return "(!" + this.left.toString()+ ")"; } };
}

/**
 * Function that creates the conditional expression (IF e e1 e2).
 * @param {Exp} - Guard of the conditional.
 * @param {Exp} - Expression if the guard is true.
 * @param {Exp} - Expression if the guard is false.
 * @return{{Exp,Exp,Exp,Exp,String}} Tuple (ty,c,t,f,s) where ty is the ITE expression, c is the guard, t is the true condition, f is the false condition and s is their string representation.
*/
function ite(c, t, f) {
    return { type: ITE, cond: c, tcase: t, fcase: f, toString: function () { return "(if " + this.cond.toString() + " then " + this.tcase.toString() + " else " + this.fcase.toString() + ")"; } };
}

/*
 ************************************************************************************************************
 Syntax: Auxiliar functions
 ************************************************************************************************************
*/

/**
 * Function that determines if an expression is arithmetic.
 * @param {Exp} - Expression to be analized.
 * @return {Bool} - True if the expression is arithmeic, false otherwise.
*/
function isArith(exp){
    switch (exp.type) {
        case NUM: 
        case VR: 
        case PLUS: 
        case TIMES: 
        case ITE:
            return true;
        default:
            return false;
    }
}

/**
 * Function that determines if an expression is an variable.
 * @param {Exp} - Expression to be analized.
 * @return {Bool} - True if the expression is arithmeic, false otherwise.
*/
function isVr(exp){
    switch (exp.type) {
        case VR: 
            return true;
        default:
            return false;
    }
}

/**
 * Function that determines if an expression is a integer value.
 * @param {Exp} - Expression to be analized.
 * @return {Bool} - True if the expression is arithmeic, false otherwise.
*/
function isNum(exp){
    switch (exp.type) {
        case NUM: 
            return true;
        default:
            return false;
    }
}

/*
 ************************************************************************************************************
 Auxiliar functions
 ************************************************************************************************************
*/

/**
 * Function that prints a text in the console.
 * @param {String} - Text that will be printed.
*/
function writeToConsole(text) {
    var csl = document.getElementById("console");
    if (typeof text == "string") {
        csl.value += text + "\n";
    } else {
        csl.value += text.toString() + "\n";
    }
}

/*
 ************************************************************************************************************
 ***** Synthetizer
 ************************************************************************************************************
*/

/**
 * Function that returns a list with all the possible expressions,
 * given a list of constructors and a list of AST.
 * @param {List} - List of constructors.
 * @param {List} - List of actual AST.
 * @return {List} - List of all the possible new AST.
*/
function grow(ops,plist){
    var listAST = [];
    //Take every actual expression.
    for(var j = 0; j < plist.length; j++){
        var e1 = plist[j];
        //If is a binary operator, take again avery actual expression, and construct the new AST.
        //Note: We have some help of the types by constructing non-illegal expressions, using a "type checking" function. 
        //For example: 'false<1' or 'x+false'.
        for(var k = 0; k < plist.length; k++){
            var e2 = plist[k];
            for(var l = 0; l < ops.length; l++){
                switch(ops[l]){
                    case PLUS:
                        if(isArith(e1) && isArith(e2)){
                            listAST.push(plus(e1,e2));
                        }
                        break;
                    case TIMES:
                        if(isArith(e1) && isArith(e2)){
                            listAST.push(times(e1,e2));
                        }
                        break;
                    case LT:
                        if(isArith(e1) && isArith(e2)){
                            listAST.push(lt(e1,e2));
                        }
                        break;
                    case AND:
                        if(!isArith(e1) && !isArith(e2)){
                            listAST.push(and(e1,e2));
                        }
                        break;
                }
            }
        }
        //Once that all the binary expressions have been constructed, construct the unary expressions.
        if(ops.includes(NOT) && !isArith(e1)){
            listAST.push(not(e1));
        }
        //The worst case (execution complexity), construct the if-sentence. 
        //Verify that the conditional is boolean, if this is correct, then took all the arithmetic expressions
        //and create all the possible ifs results, i.e., all the true executions and false executions.
        if(ops.includes(ITE) && !isArith(e1)){
            for(k = 0; k < plist.length; k++){
                e2 = plist[k];
                for(l = 0; l < plist.length; l++){
                    e3 = plist[l];
                    if(isArith(e2) && isArith(e3)){
                        listAST.push(ite(e1,e2,e3));
                    }
                }
            }
        }
    }
    return plist.concat(listAST);
}

/**
 * Function that delete all the observable equivalent expressions in comparisson with a expression given as a parameter, and
 * if this last expression has more probability than another expression e placed in the list, also we remove e.
 * @param {Exp} - Expression that be comparised.
 * @param {List} - List of candidates expressions.
 * @param {List} - List of the values of variables.
 * @param {Function} - Function of probability.
 * @return {List} - List of the not observable equivalent expressions.
*/
function elimEq(exp1,plist,inputoutputs,prob){
    //Create a copy of the list that contains the candidates.
    var noEq = plist.slice();
    //For every candidate ei, we compare exp1 with ei.
    for(var i = 0; i < plist.length; i++){
        //If they are equivalent and exists in the copy, we delete it.
        if(equivalents(exp1,plist[i],inputoutputs)){
        	var expPr = plist[i];
        	//We remove those who have lower probability than exp1.
        	if(probAST(exp1,prob) > probAST(expPr,prob)){
	            var pos = noEq.indexOf(expPr);
	            if(pos != -1){
	                noEq.splice(pos,1);
	            }
	        }
        }
    }
    return noEq;
}

/**
 * Function that compute the probability of a program.
 * @param {Exp} - Expression that will be analized.
 * @param {Function} - Function of probability.
 * @return {Float} - Value of probability.
*/
function probAST(exp,prob){
    switch (exp.type) {
        case FALSE: 
        case NUM: 
        case VR: 
        	return 1;

        case PLUS:
        case TIMES:
        case LT:
        case AND: 
        	return prob(exp.left.type,0,exp.type)*prob(exp.right.type,1,exp.type)*probAST(exp.left,prob)*probAST(exp.right,prob);

        case NOT: 
        	return prob(exp.left.type,0,exp.type)*probAST(exp.left,prob);

        case ITE: 
        	return prob(exp.cond.type,0,exp.type)*prob(exp.tcase.type,1,exp.type)*prob(exp.fcase.type,1,exp.type)*probAST(exp.cond,prob)*probAST(exp.tcase,prob)*probAST(exp.fcase,prob);
    }
}

/**
 * Function that remove the equivalent expressions, according to a list of inputs.
 * @param {List} - List of the expressions that will be analized.
 * @param {List} - List of inputs/outputs.
 * @return {List} - List of representative expressions.
*/
function elimEquivalents(plist,inputoutputs,prob){
    //List that will contain all the representant expressions.
    var noEqs = [];
    //Take the first expression of the list: if it has observable equivalent expression ei in the rest of the list,
    //then delete all the expressions ei from the list.
    //Do this until we have check all the representative expressions.
    while(plist.length != 0 && plist != undefined){
        var exp1 = plist.shift();
        plist = elimEq(exp1,plist,inputoutputs,prob);
        noEqs.push(exp1);
    }
    return noEqs;
}

/**
 * Function that check if two expressions are observable equivalents.
 * @param {Exp} - Firt expression to be analized.
 * @param {Exp} - Second expression to be analized.
 * @param {List} - List of inputs.
 * @return {Bool} - True if the two expressions are observable equivalents, False in other case.
*/
function equivalents(exp1,exp2,inputoutputs){
    //We dont know if the list of values of variables is empty.
    if(inputoutputs.length == 0){
        return (interpret(exp1,[]) == interpret(exp2,[]));
    }
    //If the list of values of variables is not empty, we check every value of variable.
    var b = interpret(exp1,inputoutputs[0])==interpret(exp2,inputoutputs[0]);
    for(var i = 1; i < inputoutputs.length; i++){
        b = b && (interpret(exp1,inputoutputs[i])==interpret(exp2,inputoutputs[i]));
    }
    return b;
}

/*
 ************************************************************************************************************
 Semantics
 ************************************************************************************************************
*/

/**
 * Interpreter for the Abstract Syntax Tree (AST=.
 * @param {exp} - The expresssion that will be evaluated.
 * @param {list} - Context that store the value of the variables.
 * @return {val} Value of the interpretation.
*/
function interpret(exp, envt) {
    switch (exp.type) {
        case FALSE: return false;
        case NUM: return exp.val;
        case VR: return envt[exp.name];
        case PLUS: return interpret(exp.left, envt) + interpret(exp.right, envt);
        case TIMES: return interpret(exp.left, envt) * interpret(exp.right, envt);
        case LT: return interpret(exp.left, envt) < interpret(exp.right, envt);
        case AND: return interpret(exp.left, envt) && interpret(exp.right, envt);
        case NOT: return !interpret(exp.left, envt);
        case ITE: if (interpret(exp.cond, envt)) { return interpret(exp.tcase, envt); } else { return interpret(exp.fcase, envt); }
    }
}

/**
 * Function that determines if the evaluation of an expression satisfaces an output.
 * @param {Exp} - Expression that be evaluated.
 * @param {List} - List of inputs/outputs.
 * @retrun {Bool} - True if the evaluation of an expression satisfaces an output, False in other case.
*/
function isCorrect(exp,inputoutputs){
    var b = interpret(exp,inputoutputs[0]) == inputoutputs[0]._out;
    for(var i = 1; i < inputoutputs.length; i++){
        var eval = interpret(exp,inputoutputs[i]);
        var output = inputoutputs[i]._out;
        b = b && (eval == output);
    }
    return b;
}

/**
 * Function that execute the Buttom-up algorithm.
 * @param {Int} - Bound on the maximum depth allowed for the generated ASTs.
 * @param {List} - List of the integer AST nodes the generator is allowed to use.
 * @param {List} - List of the boolean AST nodes the generator is allowed to use.
 * @param {List} - List of all the variable names that can appear in the generated expressions.
 * @param {List} - List of all the integer constants that can appear in the generated expressions.
 * @param {List} - List of inputs/outputs to the function. Each element in the list is a map from variable names to values; the variable "_out" maps to the expected output for that input.
 * @return {Exp} - AST for an expression that satisfies all the input/output pairs. If none is found before reaching the globalBnd, then it should return the string "FAIL".
*/
function bottomUp(globalBnd, intOps, boolOps, vars, consts, inputoutputs,prob) {
    var plist = [];
    /*
     * Cause we dont know if intOps or boolOps have the operators VR, NUM or FALSE,
     * check: if they exists, then create the terminal expressions and delete the operators in the lists mentioned above.
     * The reason for this is for a robust and efficent execution.
     */
    if(intOps.includes(NUM)){
        plist = consts.map(num);
        intOps.splice(intOps.indexOf(NUM),1);
    }
    if(intOps.includes(VR)){
        plist = plist.concat(vars.map(vr));
        intOps.splice(intOps.indexOf(VR),1);
    }
    if(boolOps.includes(FALSE)){
        plist.push(flse());
        boolOps.splice(boolOps.indexOf(FALSE),1);
    }
    writeToConsole("Terminal expressions created.\n");
    // Once that we have created the terminal expressions, begin to construct the non-terminal expressions.
    // Stop when the bound has been reached.
    var i = 0;
    while(i < globalBnd){
        //Construct all the AST in the actual level.
        plist = grow(intOps.concat(boolOps),plist);
        console.log(plist);
        //Purge the observable equivalent expressions.
        plist = elimEquivalents(plist,inputoutputs,prob);
        writeToConsole("Level "+i+" reached.\n");
        //Check if one of the actual evaluation of the expressions satisfaces the outputs.
        for(var j = 0; j < plist.length; j++){
            if(isCorrect(plist[j],inputoutputs)){
                return plist[j];
            }
        }
        i++;
    }
    //Complete the body of randomExpr
    return "FAIL";
}


function run2(){
	
	function prob(child, id, parent){
		//Example of a probability function. In this case, the function
		//has uniform distributions for most things except for cases that would
		//cause either type errors or excessive symmetries.
		//You want to make sure your solution works for arbitrary probability distributions.
		
		function unif(possibilities, kind){
			if(possibilities.includes(kind)){
				return 1.0/possibilities.length;
			}else{
				return 0;
			}
		}
		
		switch(parent){
			case PLUS: 
				if(id == 0)
					return unif([NUM, VR, PLUS, TIMES, ITE], child);
				else
					return unif([NUM, VR, TIMES, ITE], child);
				break;
	        case TIMES: 
	        	if(id == 0)
					return unif([NUM, VR, PLUS, TIMES, ITE], child);
				else
					return unif([NUM, VR, ITE], child);
	        	break;	        	
	        case LT: 
	        	return unif([NUM, VR, PLUS, TIMES, ITE], child);
	        	break;
	        case AND:
	        	return unif([LT, AND, NOT], child);
	        	break;
	        case NOT:
	        	return unif([LT, AND], child);
	        	break;
	        case ITE:
	        	if(id == 0)
	        		return unif([LT, AND], child);					
				else
					return unif([NUM, VR, PLUS, TIMES, ITE], child);
	        	break;
	        default:
	        	return 0;
		}
	}
	
	
	var rv = bottomUp(3, [VR, NUM, PLUS, TIMES, ITE], 
			             [AND, NOT, LT, FALSE], ["x", "y"], [0,1, 5], 
			             [{x:5,y:10, _out:5},{x:8,y:3, _out:3}],
			             prob
	);
	writeToConsole("RESULT: " + rv.toString());
	
}