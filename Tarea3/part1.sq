--
-- Script with the solution of Problem 1 a)
--
-- Students:
-- Galicia Mendoza, Fernando Abigail
-- Carrillo Verduzco, Diego
--
-- Email's:
-- fernandogamen@ciencias.unam.mx
-- dixego@ciencias.unam.mx
--
-- Signature: Sintesis de Programas
-- School cycle: 2019-1
-- Professor: Dr. Armando Solar-Lezama
-- Program: Maestria en Ciencia e Ingenieria de la Computacion
-- Institute: Instituto de Investigaciones en Matematicas Aplicadas y Sistemas
--

-------------------------------------------------------------------------------------------------
-- DATA TYPES
-------------------------------------------------------------------------------------------------

-- BST. Implementation of Binary Search Tree (BST).
data BST a where
    Empty :: BST a
    Node  :: x: a -> l: BST {a | _v < x} -> r: BST {a | x < _v} -> BST a

-- SortedList. Implementation of sorted list in descending order.
data SortedList a where
    Nil :: SortedList a
    Cons:: x: a -> xs:SortedList { a | _v < x } -> SortedList a

-------------------------------------------------------------------------------------------------
-- MEASURES
-------------------------------------------------------------------------------------------------

-- height. Termination measure according to a BST
termination measure height :: BST a -> {Int | _v >= 0} where
    Empty -> 0
    Node x l r -> 1 + if (height l) < (height r) then (height r) else (height l)

-- keys. Measure that gets all the keys stored in a BST.
measure keys :: BST a -> Set a where
    Empty -> []
    Node x l r -> keys l + keys r + [x]

-- vals. Measure that gets all the keys stored in a sorted list.
measure vals :: SortedList a -> Set a where
    Nil -> []    
    Cons x xs -> vals xs + [x]

-------------------------------------------------------------------------------------------------
-- FUNCTIONS
-------------------------------------------------------------------------------------------------

-- merge. Function that takes as input two sorted lists and produces a merged sorted list that 
-- contains all the elements in the two input lists.
merge :: l1: SortedList a -> l2: SortedList a -> { SortedList a |  vals _v == vals l1 + vals l2 }

-- transform. Function that transform a BST to a Sorted List.
transform :: t: BST a -> {SortedList a | vals _v == keys t}
transform = ??