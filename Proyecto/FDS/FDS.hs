module FDS where

-- | AE. Type that represents the arithmetic expressions (AE).
data AE = V String | N Int | Plus AE AE | Prod AE AE deriving(Show,Eq)

-- | Vars. Type that represents the variables of th FDS.
-- | Var "n" e represent n = e
-- | Pi i represent π = li
-- | PI represent π' = π
data Vars = Var String AE | Pi Int | Pi' Int deriving(Show,Eq)

-- | FDS. Data structure to represent the Fair Discrete Systems.
data FDS = FDS { v :: [String], ϑ :: [Vars], ϱ :: [[Vars]], j :: [Vars], c :: [(Vars,Vars)]} deriving(Show,Eq)

-- | eaC. Function that takes an AE and return an AE in C without pointers in the variables.
eaC :: AE -> String
eaC (V s) = s
eaC (N n) = show n
eaC (Plus e1 e2) = eaC e1 ++ "+" ++ eaC e2
eaC (Prod e1 e2) = eaC e1 ++ "*" ++ eaC e2

-- | eaeC. Function that takes an AE and return an AE in C with pointers in the variables.
ea2C :: AE -> String
ea2C (V s) = "*"++s
ea2C (N n) = show n
ea2C (Plus e1 e2) = ea2C e1 ++ "+" ++ ea2C e2
ea2C (Prod e1 e2) = ea2C e1 ++ "*" ++ ea2C e2

-- | consMain'. Function that construct the lines of the main representing the preconditions of our FDS.
consMain' :: [Vars] -> String
consMain' [] = ""
consMain' (v:vs) = case v of
  Var s e -> "int "++s++"="++eaC e++";\n"++consMain' vs
  otherwise -> consMain' vs

-- | line. Function that construct the new lines of corresponding to the conjuctions of the transition relation of the FDS.
line :: [Vars] -> String
line [] = ""
line (Pi i:(Pi' j:_)) = "L"++show i++":return;\n"
line (v:vs) = case v of
  Pi i -> "L"++show i++":"++line vs
  Var s e -> "*"++init s++"="++ea2C e++";"++line vs
  Pi' i -> "goto L"++show i++";\n"

-- | linesC. Function that appends all the conjuctions of the transition relation of the FDS.
linesC :: [[Vars]] -> String
linesC [] = ""
linesC (v:vs) = line v++linesC vs

-- | programPar. Function that construct the parameters of the FDS.
programPar :: [String] -> String
programPar [] = ""
programPar [v] = "int* "++v
programPar (v:vs) = "int* "++v++","++programPar vs

-- | programSig. Function that construct the signature of the FDS.
programSig :: [String] -> String
programSig vs = "program("++programPar vs++")"

-- | programB. Function that construct the type of the FDS.
programB :: [String] -> [[Vars]] -> String
programB vs vss = "void "++programSig vs++"{\n"++linesC vss++"}\n"

-- | programP2. Function that construct the dereferecing variables.
programP2 :: [String] -> String
programP2 [] = []
programP2 [v] = "&"++v
programP2 (v:vs) = "&"++v++","++programP2 vs

-- | programSig2. Function that construct the call of the FDS.
programSig2 :: [String] -> String
programSig2 vs = "program("++programP2 vs++");"

-- | consMain. Function that returns the program C implementing th FDS.
consMain :: FDS -> String
consMain fds = "#include <stdio.h>\n"++programB (v fds) (ϱ fds)++"\nint main(){\n"++consMain' (ϑ fds)++programSig2 (v fds)++"\n}\n"

--Examples
--fds1 = FDS { v = ["n"], ϑ = [Pi 0,Var "n" (N 10)], ϱ = [[Pi 0, Var "n'" (N 1),Pi' 1],[Pi 1, Var "n'" (N 10),Pi' 2],[Pi 2,Pi' 2, Var "n'" (V "n")]], j = [] , c = []}
fds1 = FDS { v = ["n","m","tmp"], ϑ = [Pi 0,Var "n" (N 5), Var "m" (N 6),Var "tmp" (N 0)], ϱ = [[Pi 0, Var "tmp'" (V "n"),Pi' 1],
                                                                                                [Pi 1, Var "n'" (V "m"),Pi' 2],
                                                                                                [Pi 2, Var "m'" (V "tmp"),Pi' 3],
                                                                                                [Pi 3,Pi' 3, Var "n'" (V "n"),Var "m'" (V "m"),Var "tmp'" (V "tmp")]], j = [] , c = []}
