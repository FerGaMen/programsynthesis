module Main where

import FDS
import System.Environment
import System.IO

main = do
  [f] <- getArgs
  handle <- openFile f ReadWriteMode
  hClose handle
  writeFile f (consMain fds1)
  return ()
