#include <stdio.h>

void program(int* n,int* m,int* tmp){
 L0: *tmp = *n;goto L1;
 L1: *n = *m;goto L2;
 L2: *m = *tmp;goto L3;
 L3: return;
}

int main(){
  int n1 = 5;
  int m1 = 6;
  int tmp1 = 0;
  program(&n1,&m1,&tmp1);
  printf("Result n:%d\n",n1);
  printf("Result m:%d\n",m1);
}
