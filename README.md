# Curso: Síntesis de Programas

In this repository are the solutions of the homeworks of the student Fernando Galicia, at the course *Síntesis de Programas*.

## Signature Data
* Signature: Sintesis de Programas
* Scholar cycle: 2019-1
* Proffesor: Dr. Armando Solar Lezama
* Program: *Maestria en Ciencia e Ingeniera de la Computacion (MCC)*
* Institute: *Instituto de Investigaciones en Matematicas Aplicadas y Sistemas (IIMAS)*
* University: *Universidad Nacional Autónoma de México (UNAM)*

## Student Data
* Name: Fernando Abigail Galicia Mendoza
* School Account Number: 308080777
* E-mail: fernandogamen@ciencias.unam.mx