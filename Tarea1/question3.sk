/**
 * Script with the grammar of Problem 1, and some expressions synthetised.
 *
 * Student: Fernando Abigail Galicia Mendoza
 * E-mail: fernandogamen@ciencias.unam.mx>
 *
 * Signature: Sintesis de Programas
 * School cycle: 2019-1
 * Proffesor: Dr. Armando Solar Lezama
 * Program: Maestria en Ciencia e Ingeniera de la Computacion
 * Institute: Instituto de Investigaciones en Matematicas Aplicadas y Sistemas
*/

/*
* Flags to make more faster the synthesis:
* 1. --slv-nativeints: Not use the unary representation of integers, for the SAT solver.
* 2. --slv--parallel: If the machine have more than one CPU, then use n-1 CPU where n is the
* number of CPU's.
* 3. --bnd-unroll-amnt: Only unfold 10 times the cycles.
*/
pragma options "--slv-nativeints --slv-parallel --bnd-unroll-amnt 10";

/*
* Generator function that creates the space of programs of boolean expressions.
*/
generator bit bexp([int nvars, int nconsts], int bnd, int[nvars] vars, int[nconsts] consts){
    assert bnd >= 0; //Make sure that the bound is an positive integer.

    if(??){return 0;} //Create the false value.

    //Create the posible boolean subexpressions.
    bit e1 = bexp(bnd-1,vars,consts);
    bit e2 = bexp(bnd-1,vars,consts);

    //Finally, create the final boolean expressions.
    if(??){return !bexp(bnd-1,vars,consts);}
    if(??){return exp(bnd-1,vars,consts) < exp(bnd-1,vars,consts);}
    if(??){return bexp(bnd-1,vars,consts) && bexp(bnd-1,vars,consts);}

}

/*
* Generator function that creates the space of programs of arithmetic expressions.
*/
generator int exp([int nvars, int nconsts], int bnd, int[nvars] vars, int[nconsts] consts){
    assert bnd >= 0;

    //We ask for the index that we need.
    int i = ??;
    if(??){return vars[i];}
    if(??){return consts[i];}

    int e1 = exp(bnd-1,vars,consts);
    int e2 = exp(bnd-1,vars,consts);
    bit c = bexp(bnd-1,vars,consts);
    if(??){return e1+e2;}
    if(??){return e1*e2;}
    if(c){return e1;}{return e2;}
}

// Diferent experiments.

/*
* Function that return the design of the first program.
*/
harness int experiment1(int x,int y){
    return exp(3,{x,y},{});
}

/*
* Function that return the design of the second program.
*/
harness int experiment2(int x,int y){
   return exp(3,{x,y},{-1});
}

/*
* Function main, shows the programs.
*/
harness void main(){
    assert experiment1(5,5) == 15;
    assert experiment1(8,3) == 14;
    assert experiment1(1234,227) == 1688;

    assert experiment2(10,7) == 17;
    assert experiment2(4,7) == -7;
    assert experiment2(10,3) == 13;
    assert experiment2(1,-7) == -6;
    assert experiment2(1,8) == -8;
}