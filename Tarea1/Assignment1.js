/**
 * @fileoverview Assigment1, Script that consists of the elements to implement
 * the Buttom-Up agoritm for the case of Arithmetic-Boolean expressions.
 *
 * @version 0.1
 *
 * @author Fernando Abigail Galicia Mendoza <fernandogamen@ciencias.unam.mx>
 *
 * Course: Sintesis de Programas
 * School cycle: 2019-1
 * Proffesor: Dr. Armando Solar Lezama
 * Program: Maestria en Ciencia e Ingeniera de la Computacion
 * Institute: Instituto de Investigaciones en Matematicas Aplicadas y Sistemas
*/

/*
 ************************************************************************************************************
 ***** Syntax
 ************************************************************************************************************
*/

// Variables to identify the constructors of the language.
var NUM = "NUM";
var FALSE = "FALSE";
var VR = "VAR";
var PLUS = "PLUS";
var TIMES = "TIMES";
var LT = "LT";
var AND = "AND";
var NOT = "NOT";
var ITE = "ITE";

var ALLOPS = [NUM, FALSE, VR, PLUS, TIMES, LT, AND, NOT, ITE];

/**
 * Function that recibes an object, and return their string representation.
 * @param {Object} - Object that will be represented.
 * @return {String} String representation of the object.
*/
function str(obj) { return JSON.stringify(obj); }

/*
 ************************************************************************************************************
 Syntax: Constructors
 ************************************************************************************************************
*/

/**
 * Function that creates the false expression (FALSE).
 * @return{{Exp,String}} Pair (t,s) where t is the False expression and s is their string representation.
*/
function flse() {
    return { type: FALSE, toString: function () { return "false"; } };
}

/**
 * Function that creates the variable expression (VR n).
 * @param {String} - Name of the variable.
 * @return{{Exp,String,String}} Tuple (t,n,s) where t is the VR expression, n is their name and s is their string representation.
*/
function vr(name) {
    return { type: VR, name: name, toString: function () { return this.name; } };
}

/**
 * Function that creates the number expression (NUM n).
 * @param {String} - Name of the variable.
 * @return{{Exp,Int,String}} Tuple (t,n,s) where t is the NUM expression, n is their value and s is their string representation.
*/
function num(n) {
    return { type: NUM, val: n, toString: function () { return this.val; } };
}

/**
 * Function that creates the plus expression (PLUS e1 e2).
 * @param {Exp} - First operand of the plus.
 * @param {Exp} - Second operand of the plus.
 * @return{{Exp,Exp,Exp,String}} Tuple (t,x,y,s) where t is the PLUS expression, x is their first operand, y is their second operand and s is their string representation.
*/
function plus(x, y) {
    return { type: PLUS, left: x, right: y, toString: function () { return "("+ this.left.toString() + "+" + this.right.toString()+")"; } };
}

/**
 * Function that creates the product expression (TIMES e1 e2).
 * @param {Exp} - First operand of the product.
 * @param {Exp} - Second operand of the product.
 * @return{{Exp,Exp,Exp,String}} Tuple (t,x,y,s) where t is the TIMES expression, x is their first operand, y is their second operand and s is their string representation.
*/
function times(x, y) {
    return { type: TIMES, left: x, right: y, toString: function () { return "(" + this.left.toString() + "*" + this.right.toString() + ")"; } };
}

/**
 * Function that creates the lower than expression (LT e1 e2).
 * @param {Exp} - First operand of the lower relation.
 * @param {Exp} - Second operand of the lower than relation.
 * @return{{Exp,Exp,Exp,String}} Tuple (t,x,y,s) where t is the LT expression, x is their first operand, y is their second operand and s is their string representation.
*/
function lt(x, y) {
    return { type: LT, left: x, right: y, toString: function () { return "(" + this.left.toString() + "<" + this.right.toString() + ")"; } };
}

/**
 * Function that creates the conjuction expression (AND e1 e2).
 * @param {Exp} - First operand of the conjuction.
 * @param {Exp} - Second operand of the conjuction.
 * @return{{Exp,Exp,Exp,String}} Tuple (t,x,y,s) where t is the AND expression, x is their first operand, y is their second operand and s is their string representation.
*/
function and(x, y) {
    return { type: AND, left: x, right: y, toString: function () { return "(" + this.left.toString() + "&&" + this.right.toString() + ")"; } };
}

/**
 * Function that creates the not expression (NOT e).
 * @param {Exp} - Operand of the not.
 * @return{{Exp,Exp,String}} Tuple (t,x,s) where t is the NOT expression, x is their operand and s is their string representation.
*/
function not(x) {
    return { type: NOT, left: x, toString: function () { return "(!" + this.left.toString()+ ")"; } };
}

/**
 * Function that creates the conditional expression (IF e e1 e2).
 * @param {Exp} - Guard of the conditional.
 * @param {Exp} - Expression if the guard is true.
 * @param {Exp} - Expression if the guard is false.
 * @return{{Exp,Exp,Exp,Exp,String}} Tuple (ty,c,t,f,s) where ty is the ITE expression, c is the guard, t is the true condition, f is the false condition and s is their string representation.
*/
function ite(c, t, f) {
    return { type: ITE, cond: c, tcase: t, fcase: f, toString: function () { return "(if " + this.cond.toString() + " then " + this.tcase.toString() + " else " + this.fcase.toString() + ")"; } };
}

/*
 ************************************************************************************************************
 Syntax: Auxiliar functions
 ************************************************************************************************************
*/

/**
 * Function that determines if an expression is arithmetic.
 * @param {Exp} - Expression to be analized.
 * @return {Bool} - True if the expression is arithmeic, false otherwise.
*/
function isArith(exp){
    switch (exp.type) {
        case NUM: 
        case VR: 
        case PLUS: 
        case TIMES: 
        case ITE:
            return true;
        default:
            return false;
    }
}

/**
 * Function that determines if an expression is an variable.
 * @param {Exp} - Expression to be analized.
 * @return {Bool} - True if the expression is arithmeic, false otherwise.
*/
function isVr(exp){
    switch (exp.type) {
        case VR: 
            return true;
        default:
            return false;
    }
}

/**
 * Function that determines if an expression is a integer value.
 * @param {Exp} - Expression to be analized.
 * @return {Bool} - True if the expression is arithmeic, false otherwise.
*/
function isNum(exp){
    switch (exp.type) {
        case NUM: 
            return true;
        default:
            return false;
    }
}

/*
 ************************************************************************************************************
 ***** Semantics
 ************************************************************************************************************
*/

/**
 * Interpreter for the Abstract Syntax Tree (AST=.
 * @param {exp} - The expresssion that will be evaluated.
 * @param {list} - Context that store the value of the variables.
 * @return {val} Value of the interpretation.
*/
function interpret(exp, envt) {
    //Recursively analize the expression and return their value.
    switch (exp.type) {
        case FALSE: return false;
        case NUM: return exp.val;
        case VR: return envt[exp.name]; //According to the context, we search the value of the variable.
        case PLUS: return interpret(exp.left, envt) + interpret(exp.right, envt);
        case TIMES: return interpret(exp.left, envt) * interpret(exp.right, envt);
        case LT: return interpret(exp.left, envt) < interpret(exp.right, envt);
        case AND: return interpret(exp.left, envt) && interpret(exp.right, envt);
        case NOT: return !interpret(exp.left, envt);
        case ITE: if (interpret(exp.cond, envt)) { return interpret(exp.tcase, envt); } else { return interpret(exp.fcase, envt); }
    }
}

/**
 * Function that determines if the evaluation of an expression satisfaces an output.
 * @param {Exp} - Expression that be evaluated.
 * @param {List} - List of inputs/outputs.
 * @retrun {Bool} - True if the evaluation of an expression satisfaces an output, False in other case.
*/
function isCorrect(exp,inputoutputs){
    var b = interpret(exp,inputoutputs[0]) == inputoutputs[0]._out;
    for(var i = 1; i < inputoutputs.length; i++){
        var eval = interpret(exp,inputoutputs[i]);
        var output = inputoutputs[i]._out;
        b = b && (eval == output);
    }
    return b;
}

/*
 ************************************************************************************************************
 General auxiliar functions
 ************************************************************************************************************
*/

/**
* Function that returns the first element that is in the intersection of two arrays.
* In the case that the intersection is empyt, returns an empty array.
* @param {List} - List of elements.
* @param {List} - List of elements.
* @param {List} - List with the first element of the intersection.
*/
function intersection(array1,array2){
    var inter = array1.slice();
    for(var i = 0; i < array2.length; i++){
        if(array1.indexOf(array2[i]) != -1){
            return [array1[i]];
        }
    }
    return [];
}

/**
 * Function that returns a random number bounded by two integers.
 * @param {int} - Lower bound.
 * @param {int} - Upper bound.
 * @return {int} The random integer number.
*/
function randInt(lb, ub) {
    var rf = Math.random();
    rf = rf * (ub - lb) + lb;
    return Math.floor(rf);
}

/**
 * Function that returns a random element of a list.
 * @param {list} - List of elements.
 * @return {Object} Random element of the list.
*/
function randElem(from) {
    return from[randInt(0, from.length)];
}

/**
 * Function that prints a text in the console.
 * @param {String} - Text that will be printed.
*/
function writeToConsole(text) {
    var csl = document.getElementById("console");
    if (typeof text == "string") {
        csl.value += text + "\n";
    } else {
        csl.value += text.toString() + "\n";
    }
}

/*
 ************************************************************************************************************
 ***** Synthetizer
 ************************************************************************************************************
*/

/**
 * Function that returns a list with all the possible expressions,
 * given a list of constructors and a list of AST.
 * @param {List} - List of constructors.
 * @param {List} - List of actual AST.
 * @return {List} - List of all the possible new AST.
*/
function grow(ops,plist){
    var listAST = [];
    //Take every actual expression.
    for(var j = 0; j < plist.length; j++){
        var e1 = plist[j];
        //If is a binary operator, take again avery actual expression, and construct the new AST.
        //Note: We have some help of the types by constructing non-illegal expressions, using a "type checking" function. 
        //For example: 'false<1' or 'x+false'.
        for(var k = 0; k < plist.length; k++){
            var e2 = plist[k];
            for(var l = 0; l < ops.length; l++){
                switch(ops[l]){
                    case PLUS:
                        if(isArith(e1) && isArith(e2)){
                            listAST.push(plus(e1,e2));
                        }
                        break;
                    case TIMES:
                        if(isArith(e1) && isArith(e2)){
                            listAST.push(times(e1,e2));
                        }
                        break;
                    case LT:
                        if(isArith(e1) && isArith(e2)){
                            listAST.push(lt(e1,e2));
                        }
                        break;
                    case AND:
                        if(!isArith(e1) && !isArith(e2)){
                            listAST.push(and(e1,e2));
                        }
                        break;
                }
            }
        }
        //Once that all the binary expressions have been constructed, construct the unary expressions.
        if(ops.includes(NOT) && !isArith(e1)){
            listAST.push(not(e1));
        }
        //The worst case (execution complexity), construct the if-sentence. 
        //Verify that the conditional is boolean, if this is correct, then took all the arithmetic expressions
        //and create all the possible ifs results, i.e., all the true executions and false executions.
        if(ops.includes(ITE) && !isArith(e1)){
            for(k = 0; k < plist.length; k++){
                e2 = plist[k];
                for(l = 0; l < plist.length; l++){
                    e3 = plist[l];
                    if(isArith(e2) && isArith(e3)){
                        listAST.push(ite(e1,e2,e3));
                    }
                }
            }
        }
    }
    return plist.concat(listAST);
}

/**
 * Function that returns a list with all the possible expressions,
 * given a list of constructors and a list of AST.
 * Under the conditions that:
 * 1. Multiplications can only occur between variables and constants or between two variables.
 * 2. Comparisons cannot include any arithmetic, only variables and constants.
 * @param {List} - List of constructors.
 * @param {List} - List of actual AST.
 * @return {List} - List of all the possible new AST.
*/
function growFaster(ops,plist){
    var listAST = [];
    //Take every actual expression.
    for(var j = 0; j < plist.length; j++){
        var e1 = plist[j];
        //If is a binary operator, take again avery actual expression, and construct the new AST.
        //Note: We have some help of the types by constructing non-illegal expressions, using a "type checking" function. 
        //For example: 'false<1' or 'x+false'.
        for(var k = 0; k < plist.length; k++){
            var e2 = plist[k];
            for(var l = 0; l < ops.length; l++){
                switch(ops[l]){
                    case PLUS:
                        if(isArith(e1) && isArith(e2)){
                            listAST.push(plus(e1,e2));
                        }
                        break;
                    case TIMES:
                        var cond = (isVr(e1) && isNum(e1)) || (isNum(e1) && isVr(e2)) || (isVr(e1) && isVr(e2));
                        if(cond){
                            listAST.push(times(e1,e2));
                        }
                        break;
                    case LT:
                        var cond = (isVr(e1) && isNum(e1)) || (isNum(e1) && isVr(e2)) || 
                                   (isVr(e1) && isVr(e2)) || (isNum(e1) && isNum(e2));
                        if(cond){
                            listAST.push(lt(e1,e2));
                        }
                        break;
                    case AND:
                        if(!isArith(e1) && !isArith(e2)){
                            listAST.push(and(e1,e2));
                        }
                        break;
                }
            }
        }
        //Once that all the binary expressions have been constructed, construct the unary expressions.
        if(ops.includes(NOT) && !isArith(e1)){
            listAST.push(not(e1));
        }
        //The worst case (execution complexity), construct the if-sentence. 
        //Verify that the conditional is boolean, if this is correct, then took all the arithmetic expressions
        //and create all the possible ifs results, i.e., all the true executions and false executions.
        if(ops.includes(ITE) && !isArith(e1)){
            for(k = 0; k < plist.length; k++){
                e2 = plist[k];
                for(l = 0; l < plist.length; l++){
                    e3 = plist[l];
                    if(isArith(e2) && isArith(e3)){
                        listAST.push(ite(e1,e2,e3));
                    }
                }
            }
        }
    }
    return listAST;
}

/**
 * Function that check if two expressions are observable equivalents.
 * @param {Exp} - Firt expression to be analized.
 * @param {Exp} - Second expression to be analized.
 * @param {List} - List of inputs.
 * @return {Bool} - True if the two expressions are observable equivalents, False in other case.
*/
function equivalents(exp1,exp2,inputoutputs){
    //We dont know if the list of values of variables is empty.
    if(inputoutputs.length == 0){
        return (interpret(exp1,[]) == interpret(exp2,[]));
    }
    //If the list of values of variables is not empty, we check every value of variable.
    var b = interpret(exp1,inputoutputs[0])==interpret(exp2,inputoutputs[0]);
    for(var i = 1; i < inputoutputs.length; i++){
        b = b && (interpret(exp1,inputoutputs[i])==interpret(exp2,inputoutputs[i]));
    }
    return b;
}

/**
 * Function that delete all the observable equivalent expressions in comparisson with a expression given as a parameter.
 * @param {Exp} - Expression that be comparised.
 * @param {List} - List of candidates expressions.
 * @param {List} - List of the values of variables.
 * @return {List} - List of the not observable equivalent expressions.
*/
function elimEq(exp1,plist,inputoutputs){
    //Create a copy of the list that contains the candidates.
    var noEq = plist.slice();
    //For every candidate ei, we compare exp1 with ei.
    for(var i = 0; i < plist.length; i++){
        //If they are equivalent and exists in the copy, we delete it.
        if(equivalents(exp1,plist[i],inputoutputs)){
            var pos = noEq.indexOf(plist[i]);
            if(pos != -1){
                noEq.splice(pos,1);
            }
        }
    }
    return noEq;
}

/**
 * Function that remove the equivalent expressions, according to a list of inputs.
 * @param {List} - List of the expressions that will be analized.
 * @param {List} - List of inputs/outputs.
 * @return {List} - List of representative expressions.
*/
function elimEquivalents(plist,inputoutputs){
    //List that will contain all the representant expressions.
    var noEqs = [];
    //Take the first expression of the list: if it has observable equivalent expression ei in the rest of the list,
    //then delete all the expressions ei from the list.
    //Do this until we have check all the representative expressions.
    while(plist.length != 0 && plist != undefined){
        var exp1 = plist.shift();
        plist = elimEq(exp1,plist,inputoutputs);
        noEqs.push(exp1);
    }
    return noEqs;
}

/**
 * Function that execute the Buttom-up algorithm.
 * @param {Int} - Bound on the maximum depth allowed for the generated ASTs.
 * @param {List} - List of the integer AST nodes the generator is allowed to use.
 * @param {List} - List of the boolean AST nodes the generator is allowed to use.
 * @param {List} - List of all the variable names that can appear in the generated expressions.
 * @param {List} - List of all the integer constants that can appear in the generated expressions.
 * @param {List} - List of inputs/outputs to the function. Each element in the list is a map from variable names to values; the variable "_out" maps to the expected output for that input.
 * @return {Exp} - AST for an expression that satisfies all the input/output pairs. If none is found before reaching the globalBnd, then it should return the string "FAIL".
*/
function bottomUp(globalBnd, intOps, boolOps, vars, consts, inputoutputs) {
    var plist = [];
    /*
     * Cause we dont know if intOps or boolOps have the operators VR, NUM or FALSE,
     * check: if they exists, then create the terminal expressions and delete the operators in the lists mentioned above.
     * The reason for this is for a robust and efficent execution.
     */
    if(intOps.includes(NUM)){
        plist = consts.map(num);
        intOps.splice(intOps.indexOf(NUM),1);
    }
    if(intOps.includes(VR)){
        plist = plist.concat(vars.map(vr));
        intOps.splice(intOps.indexOf(VR),1);
    }
    if(boolOps.includes(FALSE)){
        plist.push(flse());
        boolOps.splice(boolOps.indexOf(FALSE),1);
    }
    writeToConsole("Terminal expressions created.\n");
    // Once that we have created the terminal expressions, begin to construct the non-terminal expressions.
    // Stop when the bound has been reached.
    var i = 0;
    while(i < globalBnd){
        //Construct all the AST in the actual level.
        plist = grow(intOps.concat(boolOps),plist);
        //Purge the observable equivalent expressions.
        plist = elimEquivalents(plist,inputoutputs);
        writeToConsole("Level "+i+" reached.\n");
        //Check if one of the actual evaluation of the expressions satisfaces the outputs.
        for(var j = 0; j < plist.length; j++){
            if(isCorrect(plist[j],inputoutputs)){
                return plist[j];
            }
        }
        i++;
    }
    //Complete the body of randomExpr
    return "FAIL";
}

/**
 * Function that execute the Buttom-up algorithm optimized.
 * The optimization consists in two aspects:
 * 1. Multiplications can only occur between variables and constants or between two variables.
 * 2. Comparisons cannot include any arithmetic, only variables and constants.
 * @param {Int} - Bound on the maximum depth allowed for the generated ASTs.
 * @param {List} - List of the integer AST nodes the generator is allowed to use.
 * @param {List} - List of the boolean AST nodes the generator is allowed to use.
 * @param {List} - List of all the variable names that can appear in the generated expressions.
 * @param {List} - List of all the integer constants that can appear in the generated expressions.
 * @param {List} - List of inputs/outputs to the function. Each element in the list is a map from variable names to values; the variable "_out" maps to the expected output for that input.
 * @return {Exp} - AST for an expression that satisfies all the input/output pairs. If none is found before reaching the globalBnd, then it should return the string "FAIL".
*/
function bottomUpFaster(globalBnd, intOps, boolOps, vars, consts, inputoutputs){
    var plist = [];
    /*
     * Cause we dont know if intOps or boolOps have the operators VR, NUM or FALSE,
     * check: if they exists, then create the terminal expressions and delete the operators in the lists mentioned above.
     * The reason for this is for a robust and efficent execution.
     */
    if(intOps.includes(NUM)){
        plist = consts.map(num);
        intOps.splice(intOps.indexOf(NUM),1);
    }
    if(intOps.includes(VR)){
        plist = plist.concat(vars.map(vr));
        intOps.splice(intOps.indexOf(VR),1);
    }
    if(boolOps.includes(FALSE)){
        plist.push(flse());
        boolOps.splice(boolOps.indexOf(FALSE),1);
    }
    writeToConsole("Terminal expressions created.\n");
    // Once that we have created the terminal expressions, begin to construct the non-terminal expressions.
    // Stop when the bound has been reached.
    var i = 0;
    while(i < globalBnd){
        //Construct all the AST in the actual level.
        plist = growFaster(intOps.concat(boolOps),plist);
        //Purge the observable equivalent expressions.
        plist = elimEquivalents(plist,inputoutputs);
        writeToConsole("Level "+i+" reached.\n");
        //Check if one of the actual evaluation of the expressions satisfaces the outputs.
        for(var j = 0; j < plist.length; j++){
            if(isCorrect(plist[j],inputoutputs)){
                return plist[j];
            }
        }
        i++;
    }
    //Complete the body of randomExpr
    return "FAIL";
}

/*
 ************************************************************************************************************
 Examples for Problem 1
 ************************************************************************************************************
*/

function run1a1(){
    var rv = bottomUp(3, [VR, NUM, PLUS, TIMES, ITE], [AND, NOT, LT, FALSE], ["x", "y"], [4, 5], [{x:5,y:10, _out:5},{x:8,y:3, _out:3}]);
    writeToConsole("PROGRAM: " + rv.toString());
}


function run1a2(){
    
    var rv = bottomUp(3, [VR, NUM, PLUS, TIMES, ITE], [AND, NOT, LT, FALSE], ["x", "y"], [-1, 5], [
        {x:10, y:7, _out:17},
        {x:4, y:7, _out:-7},
        {x:10, y:3, _out:13},
        {x:1, y:-7, _out:-6},
        {x:1, y:8, _out:-8}     
        ]);
    writeToConsole("PROGRAM: " + rv.toString());
    
}


function run1b(){
    
    var rv = bottomUpFaster(3, [VR, NUM, PLUS, TIMES, ITE], [AND, NOT, LT, FALSE], ["x", "y"], [-1, 5], [
        {x:10, y:7, _out:17},
        {x:4, y:7, _out:-7},
        {x:10, y:3, _out:13},
        {x:1, y:-7, _out:-6},
        {x:1, y:8, _out:-8}     
        ]);
    writeToConsole("PROGRAM: " + rv.toString());
    
}

/*
 ************************************************************************************************************
 ***** Structured Synthetizer
 ************************************************************************************************************
*/

/**
 * Function that compute the constant ?? that satisfaces the equation: 2*x+??=r.
 * @param {Int} - Variable x.
 * @param {Int} - Result of the expression 2*x+??.
 * @return {Int} - Value of ??.
 * Is O(1), is just arithmetic operations.
*/
function termC1(x,r){
    return r-2*x;
}

/**
 * Function that compute the constant ?? that satisfaces the equation: x*x+??=r.
 * @param {Int} - Variable x.
 * @param {Int} - Result of the expression 2*x+??.
 * @return {Int} - Value of ??.
 * Is O(1), is just arithmetic operations.
*/
function termC2(x,r){
    return r-x*x;
}

/**
 * Function that compute the constant ?? that satisfaces the equation: 3*x+??=r.
 * @param {Int} - Variable x.
 * @param {Int} - Result of the expression 3*x+??.
 * @return {Int} - Value of ??.
 * Is O(1), is just arithmetic operations.
*/
function termC3(x,r){
    return r-3*x;
}

/**
 * Function that returns all the possible constant given a list of inputs/outputs, according of the
 * expressions 2*x, x*x and 3*x.
 * @param {List} - List of inputs/outputs.
 * @return {List} - List of the constants that satisfaces the above equations.
*/
function findCts(inputoutputs){
    var cts = [];
    //This is order O(n), because we construct the constants of every input/output.
    for(var i = 0; i < inputoutputs.length; i++){
        var x = inputoutputs[i][0];
        var r = inputoutputs[i][1];
        //Resolving the equations
        var ct1 = termC1(x,r);
        var ct2 = termC2(x,r);
        var ct3 = termC3(x,r);
        cts.push([ct1,ct2,ct3]);
    }  
    return cts;
}

/**
 * Function that returns the constant can fill the hole according to the constant ??.
 * @param{List} - List of all possible constants that satisfaces the above equations.
 * @param{Int} - Index to begin the search.
 * @return{Object} - Object that consists in the constant and the index where last appear in the list.
*/
function ctSearched(cts,indexC){
    var cts1 = cts[indexC]; //We get the first candidate
    //Searching for all the possible candidates.
    //This search is O(n-indexC), because we take every element in the subarray [indexC,...,n] where n is the last element of
    //the constants array.
    for(var i = indexC+1; i < cts.length; i++){
        var cts2 = cts[i];
        //The intersection function is O(m*r), where m,r is the length of array1 and array2, respectively.
        //But all the arrays given in this particular case, their length is 3. So, the order is O(1). 
        var ctsInt = intersection(cts1,cts2);
        //If the sequence of constants broke, we return the previous constant.
        if(ctsInt.length <= 0 || ctsInt == undefined){
            //Same argument in the previous use of intersection.
            return {ct:intersection(cts1,cts[i-1])[0],ind:i-1};
        }
    }
    return {ct:cts1[0],ind:indexC};
}

/**
 * Function that construct an expression according a list of constants that satisfaces the above equations,
 * index of the constant that fill's the hole ??.
 * @param{List} - List of constants.
 * @param{Int} - Index of the last constant that satisfaces the above equations.
 * @param{Int} - Index of the constant ?? to construct the term.
 * @param{Exp} - Term constructed.
*/
function expr(cts,indexC,ct){
    var ctsT = cts[indexC]; //We get the list that contains the constant ??.
    var indT = ctsT.indexOf(ct); //We get the constant ??.
    //We construct the term according to the constant ??.
    switch(indT){
        case 0:
            return plus(times(num(2),vr("x")),num(cts[indexC][0]));
        case 1:
            return plus(times(vr("x"),num("x")),num(cts[indexC][1]));
        case 2:
            return plus(times(num(3),vr("x")),num(cts[indexC][2]));
    }
    return "Unkown error";
}

/**
 * Function that construct the program that satisfaces the inputs/outputs.
 * @param{List} - List of inputs/outputs.
 * @return{Exp} - Program constructed.
 * Is O(n) the explation of this is in the bellow commets.
*/
function structured(inputoutputs){

    //We get all the possible constants.
    //Is O(n), because we construct the three constants for every input.
    var cts = findCts(inputoutputs);
    
    //We get the three constants that will satisface the outputs.
    //Is O(n), because the function ctSearched is O(n-ind) cause we search for the constant in the last time
    //that the sequence of constants is broken, we call three times that implies we get O(3n-3i).
    var c1 = ctSearched(cts,0);
    var c2 = ctSearched(cts,c1.ind+1);
    var c3 = ctSearched(cts,c2.ind+1);
    var c4 = ctSearched(cts,c3.ind+1);

    //Constructing the terms that will be the branches of the if.
    //Is O(1), because we get the constant according to the index we previous calculate.
    var exp1 = expr(cts,c1.ind,c1.ct);
    var exp2 = expr(cts,c2.ind,c2.ct);
    var exp3 = expr(cts,c3.ind,c3.ct);
    var exp4 = expr(cts,c4.ind,c4.ct);

    //Construct every if sentence. Is O(1), we just constructing expressions.
    var fB = num(inputoutputs[c1.ind][0]+1);
    var sB = num(inputoutputs[c2.ind][0]+1);
    var tB = num(inputoutputs[c3.ind][0]+1);

    //Finally, we construct all the program, is O(1).
    var program = ite(lt(vr("x"),fB),
        exp1,
        ite(lt(vr("x"),sB),
            exp2,
            ite(lt(vr("x"),tB),
                exp3,
                exp4)
            ),
        );

    return program;
}

/*
 ************************************************************************************************************
 Problem 2
 ************************************************************************************************************
*/

function run2() {
    var inpt = JSON.parse(document.getElementById("input2").value);
    //We construct the program.
    var pro = structured(inpt);
    //Show in a pretty string.
    writeToConsole("Program:\n"+pro);
}


function genData() {
    //If you write a block of code in program1 that writes its output to a variable out,
    //and reads from variable x, this function will feed random inputs to that block of code
    //and write the input/output pairs to input2.
    program = document.getElementById("program1").value
    function gd(x1) {
        var term1 = times(num(2),vr("x"));
        var term2 = ite(lt(vr("x"),num(60)),plus(times(num(3),vr("x")),num(12)),term1);
        var term3 = ite(lt(vr("x"),num(35)),plus(times(vr("x"),vr("x")),num(23)),term2);
        var term = ite(lt(vr("x"),num(10)),plus(times(num(2),vr("x")),num(10)),term3);
        var out = interpret(term,{x:x1});
        return out;
    }
    textToIn = document.getElementById("input2");
    textToIn.value = "[";
    var bnd = 10;
    for(i=0; i<bnd; ++i){
        if(i!=0){ textToIn.textContent += ", "; }
        var inpt = randInt(0, 100);
        textToIn.value += "[" + inpt + ", " + gd(inpt) + "]";
        if(i<bnd-1){
            textToIn.value += ",";
        }
    }
    textToIn.value += "]";
}