/**
 * @fileoverview Assigment1, Script that consists of the elements to implement
 * the verification condition for the Sketch program.
 *
 * @version 0.1
 *
 * @author Diego Carrillo Verduzco <dixego@ciencias.unam.mx>
 * @author Fernando Abigail Galicia Mendoza <fernandogamen@ciencias.unam.mx>
 *
 * Course: Sintesis de Programas
 * School cycle: 2019-1
 * Proffesor: Dr. Armando Solar Lezama
 * Program: Maestria en Ciencia e Ingeniera de la Computacion
 * Institute: Instituto de Investigaciones en Matematicas Aplicadas y Sistemas
*/

/*
 ************************************************************************************************************
 ***** Syntax
 ************************************************************************************************************
*/

// Variables to identify the constructors of the language.
var NUM = "NUM";
var FALSE = "FALSE";
var VR = "VAR";
var PLUS = "PLUS";
var TIMES = "TIMES";
var LT = "LT";
var AND = "AND";
var NOT = "NOT";

var SEQ = "SEQ";
var IFTE = "IFSTMT";
var WHLE = "WHILESTMT";
var ASSGN = "ASSGN";
var SKIP = "SKIP";
var ASSUME = "ASSUME";
var ASSERT = "ASSERT";

//Some extra syntax for the invariants.
var INVAR = "INVAR";

var OR = "OR";
var EQ = "EQ";
var LE = "LE";

/**
 * Function that recibes an object, and return their string representation.
 * @param {Object} - Object that will be represented.
 * @return {String} String representation of the object.
*/
function str(obj) { return JSON.stringify(obj); }

//Constructor definitions for the different AST nodes.

function flse() {
    return { type: FALSE, toString: function () { return "false"; } };
}

/*
 ************************************************************************************************************
 Syntax: Constructors
 ************************************************************************************************************
*/

/**
 * Function that creates the variable expression (VR n).
 * @param {String} - Name of the variable.
 * @return{{Exp,String,String}} Tuple (t,n,s) where t is the VR expression, n is their name and s is their string representation.
*/
function vr(name) {
    return { type: VR, name: name, toString: function () { return this.name; } };
}

/**
 * Function that creates the number expression (NUM n).
 * @param {String} - Name of the variable.
 * @return{{Exp,Int,String}} Tuple (t,n,s) where t is the NUM expression, n is their value and s is their string representation.
*/
function num(n) {
    return { type: NUM, val: n, toString: function () { return this.val; } };
}

/**
 * Function that creates the plus expression (PLUS e1 e2).
 * @param {Exp} - First operand of the plus.
 * @param {Exp} - Second operand of the plus.
 * @return{{Exp,Exp,Exp,String}} Tuple (t,x,y,s) where t is the PLUS expression, x is their first operand, y is their second operand and s is their string representation.
*/
function plus(x, y) {
    return { type: PLUS, left: x, right: y, toString: function () { return "(" + this.left.toString() + "+" + this.right.toString() + ")"; } };
}

/**
 * Function that creates the product expression (TIMES e1 e2).
 * @param {Exp} - First operand of the product.
 * @param {Exp} - Second operand of the product.
 * @return{{Exp,Exp,Exp,String}} Tuple (t,x,y,s) where t is the TIMES expression, x is their first operand, y is their second operand and s is their string representation.
*/
function times(x, y) {
    return { type: TIMES, left: x, right: y, toString: function () { return "(" + this.left.toString() + "*" + this.right.toString() + ")"; } };
}

/**
 * Function that creates the lower than expression (LT e1 e2).
 * @param {Exp} - First operand of the lower relation.
 * @param {Exp} - Second operand of the lower than relation.
 * @return{{Exp,Exp,Exp,String}} Tuple (t,x,y,s) where t is the LT expression, x is their first operand, y is their second operand and s is their string representation.
*/
function lt(x, y) {
    return { type: LT, left: x, right: y, toString: function () { return "(" + this.left.toString() + "<" + this.right.toString() + ")"; } };
}

/**
 * Function that creates the conjuction expression (AND e1 e2).
 * @param {Exp} - First operand of the conjuction.
 * @param {Exp} - Second operand of the conjuction.
 * @return{{Exp,Exp,Exp,String}} Tuple (t,x,y,s) where t is the AND expression, x is their first operand, y is their second operand and s is their string representation.
*/
function and(x, y) {
    return { type: AND, left: x, right: y, toString: function () { return "(" + this.left.toString() + "&&" + this.right.toString() + ")"; } };
}

/**
 * Function that creates the disyunction expression (OR e1 e2).
 * @param {Exp} - First operand of the disyunction.
 * @param {Exp} - Second operand of the disyunction.
 * @return{{Exp,Exp,Exp,String}} Tuple (t,x,y,s) where t is the AND expression, x is their first operand, y is their second operand and s is their string representation.
*/
function or(x, y) {
    return { type: OR, left: x, right: y, toString: function () { return "(" + this.left.toString() + "||" + this.right.toString() + ")"; } };
}

/**
 * Function that creates the equal expression (EQ e1 e2).
 * @param {Exp} - First operand of the equality.
 * @param {Exp} - Second operand of the equality.
 * @return{{Exp,Exp,Exp,String}} Tuple (t,x,y,s) where t is the AND expression, x is their first operand, y is their second operand and s is their string representation.
*/
function eq(x, y) {
    return { type: EQ, left: x, right: y, toString: function () { return "(" + this.left.toString() + "==" + this.right.toString() + ")"; } };
}

/**
 * Function that creates the lower or equal expression (LE e1 e2).
 * @param {Exp} - First operand of the lower or equal.
 * @param {Exp} - Second operand of the lower or equal.
 * @return{{Exp,Exp,Exp,String}} Tuple (t,x,y,s) where t is the AND expression, x is their first operand, y is their second operand and s is their string representation.
*/
function le(x, y) {
    return { type: LE, left: x, right: y, toString: function () { return "(" + this.left.toString() + "<=" + this.right.toString() + ")"; } };
}

/**
 * Function that creates the not expression (NOT e).
 * @param {Exp} - Operand of the not.
 * @return{{Exp,Exp,String}} Tuple (t,x,s) where t is the NOT expression, x is their operand and s is their string representation.
*/
function not(x) {
    return { type: NOT, left: x, toString: function () { return "(!" + this.left.toString() + ")"; } };
}

/**
 * Function that creates the sequence expression (SEQ e1 e2).
 * @param {Exp} - First operand of the sequence.
 * @param {Exp} - Second operand of the sequence.
 * @return{{Exp,Exp,Exp,String}} Tuple (t,x,y,s) where t is the AND expression, x is their first operand, y is their second operand and s is their string representation.
*/
function seq(s1, s2) {
    return { type: SEQ, fst: s1, snd: s2, toString: function () { return "" + this.fst.toString() + "\n" + this.snd.toString(); } };
}

/**
 * Function that creates the assume expression (ASSUME e).
 * @param {Exp} - Operand of the assume.
 * @return{{Exp,Exp,String}} Tuple (t,x,s) where t is the NOT expression, x is their operand and s is their string representation.
*/
function assume(e) {
    return { type: ASSUME, exp: e, toString: function () { return "assume " + this.exp.toString() + ";"; } };
}

/**
 * Function that creates the assert expression (ASSERT e).
 * @param {Exp} - Operand of the assert.
 * @return{{Exp,Exp,String}} Tuple (t,x,s) where t is the NOT expression, x is their operand and s is their string representation.
*/
function assert(e) {
    return { type: ASSERT, exp: e, toString: function () { return "assert " + this.exp.toString() + ";"; } };
}

/**
 * Function that creates the assign expression (ASSIGN n v).
 * @param {VAR} - First operand of the assign.
 * @param {VAL} - Second operand of the assign.
 * @return{{Exp,Exp,Exp,String}} Tuple (t,x,y,s) where t is the AND expression, x is their first operand, y is their second operand and s is their string representation.
*/
function assgn(v, val) {
    return { type: ASSGN, vr: v, val: val, toString: function () { return "" + this.vr + ":=" + this.val.toString() + ";"; } };
}

/**
 * Function that creates the conditional expression (IF e e1 e2).
 * @param {Exp} - Guard of the conditional.
 * @param {Exp} - Expression if the guard is true.
 * @param {Exp} - Expression if the guard is false.
 * @return{{Exp,Exp,Exp,Exp,String}} Tuple (ty,c,t,f,s) where ty is the ITE expression, c is the guard, t is the true condition, f is the false condition and s is their string representation.
*/
function ifte(c, t, f) {
    return { type: IFTE, cond: c, tcase: t, fcase: f, toString: function () { return "if(" + this.cond.toString() + "){\n" + this.tcase.toString() + '\n}else{\n' + this.fcase.toString() + '\n}'; } };
}

/**
 * Function that creates the while expression (WHILE e1 e2).
 * @param {Exp} - First operand of the while.
 * @param {Exp} - Second operand of the while.
 * @return{{Exp,Exp,Exp,String}} Tuple (t,x,y,s) where t is the AND expression, x is their first operand, y is their second operand and s is their string representation.
*/
function whle(c, b) {
    return { type: WHLE, cond: c, body: b, toString: function () { return "while(" + this.cond.toString() + "){\n" + this.body.toString() + '\n}'; } };
}

/**
 * Function that creates the skip expression (SKIP).
 * @return{{Exp,String}} Pair (t,s) where t is the False expression and s is their string representation.
*/
function skip() {
    return { type: SKIP, toString: function () { return "/*skip*/"; } };
}

/**
 * Function that creates the invariant expression (INVARIANT [x1,...,xn]).
 * @param {Exp} - First operand of the invariant.
 * @param {[Exp]} - Second operand of the invariant.
 * @return{{Exp,Exp,Exp,String}} Tuple (t,x,y,s) where t is the AND expression, x is their first operand, y is their second operand and s is their string representation.
*/
function invariant(nme, exprs) {
  return { type: INVAR, name: nme, exprs: exprs, toString: function() { 
    var vs = this.exprs.map(v => v.toString()).join(", ")
    return this.name + "("+ vs +")";
  }};
}

/**
 * Function that creates the true expression (TRUE).
 * @return{{Exp,String}} Pair (t,s) where t is the False expression and s is their string representation.
*/
function tru() {
    return not(flse());
}

/**
 * Function that creates a block of code.
 * @param {[Exp]} - List of expressions.
 * @return{Exp} - Program block.
*/
function block(slist) {
    if (slist.length == 0) {
        return skip();
    }
    if (slist.length == 1) {
        return slist[0];
    } else {
        return seq(slist[0], block(slist.slice(1)));
    }
}

/*
 ************************************************************************************************************
 Verification Condition: AST
 ************************************************************************************************************
*/

/**
 * Function that substitute an variable for a new expression, i.e., e[x:=e']
 * @param {Exp} - Expression that be modified.
 * @param {Var} - Variable that be searched.
 * @param {Exp} - New expression.
 * @return{Exp} - Expression modified.
*/
function subst(elem, pre, post) {
  switch(elem.type) {
    case VR: 
      if (elem.name === pre)
        return post;
      else 
        return elem;

    case NOT:
      var subsl = subst(elem.left, pre, post);
      return not(subsl);

    case PLUS:
    case TIMES:
    case AND:
    case OR:
    case LT:
    case EQ:
    case LE:
      var subsl = subst(elem.left, pre, post)
      var subsr = subst(elem.right, pre, post)
      return {type: elem.type, left: subsl, right: subsr,  toString: elem.toString};

    case INVAR:
      return invariant(elem.name, elem.exprs.map(v => subst(v, pre, post)));

    case SEQ:
      return seq(subst(elem.fst, pre, post), subst(elem.snd, pre, post));

    case IFTE:
      return ifte(
        subst(elem.cond, pre, post), 
        subst(elem.tcase, pre, post), 
        subst(elem.fcase, pre, post));

    case WHLE:
      return whle(subst(elem.cond, pre, post), subst(elem.body, pre, post));

    case ASSUME:
      return assume(subst(elem.exp, pre, post));

    default: return elem;
  }
}

/**
 * Function that get de modified variables in a program.
 * @param {Exp} - Program that be analized.
 * @return{Set(Var)} - Set of variables modified.
*/
function modifiedVars(prog) {
  var modified = new Set()

  switch(prog.type) {
    //Only need to be analized the following expressions.
    case SEQ:
      return union(modifiedVars(prog.fst), modifiedVars(prog.snd));
    case IFTE:
      return union(modifiedVars(prog.tcase), modifiedVars(prog.fcase));
    case WHLE:
      return modifiedVars(prog.body);
    case ASSGN:
      return new Set([prog.vr]); //The nature of the assing, modify a variable.
    default: return modified;
  }
}

/**
 * Function that returns all the variables of a program.
 * @param {Exp} - Program that be analized.
 * @return{Set(Var)} - Variables in a program.
*/
function getVars(prog) {
  var vars = new Set()

  switch(prog.type) {
    case ASSERT: 
    case ASSUME: 
      return union(vars, getVars(prog.exp));

    case ASSGN: 
      vars.add(prog.vr);
      return union(vars, getVars(prog.val));
    case IFTE:
      var condVars = getVars(prog.cond);
      var tVars = getVars(prog.tcase);
      var fVars = getVars(prog.fcase);
      return union(vars, union(condVars, union(tVars, fVars)));
    case WHLE:
      var condVars = getVars(prog.cond);
      var bodyVars = getVars(prog.body);
      return union(vars, union(condVars, bodyVars));

    case SEQ: 
      return union(getVars(prog.fst), getVars(prog.snd));
    
    case VR:
      vars.add(prog.name)
      return vars; break;
    
    case PLUS:
    case TIMES:
    case LT:
    case AND:
    case OR:
    case EQ:
    case LE:
      var varsLeft = getVars(prog.left)
      var varsRight = getVars(prog.right)
      return union(varsLeft, varsRight);

    case NOT:
      var varsLeft = getVars(prog.left)
      return varsLeft;

    case INVAR:
      //INVAR is a constructor that contains a list of variables, we need to analize every element of that list.
      return prog.exprs.map(e => getVars(e)).reduce((x,y) => union(x,y), vars);

    default:
      return vars;
  }
}

/**
 * Function that returns the union of two sets.
 * @param {Set} - First set.
 * @param {Set} - Second set.
 * @return{Set} - Union of the sets.
*/
function union(setA, setB) {
  var u = new Set(setA)
  setB.forEach(b => u.add(b));
  return u;
}

/**
 * Auxiliar function that compute the Verification Condition.
 * @param {Exp} - Program that be analized.
 * @param {Exp} - Invariant condition.
 * @param {Int} - Number of the variables in the invariant.
 * @param {[Vars]} - Variables in the invariant.
 * @return{Exp} - Verification Condition.
*/
function computeVcAux(prog, cond, invs, vars) {
  switch(prog.type) {
    case SKIP: 
      return cond;
    case ASSGN:
      return subst(cond, prog.vr, prog.val);
    case ASSERT:
      return seq(prog.exp, cond);
    case ASSUME:
      return ifte(prog.exp, cond, tru());
    case SEQ:
      var vcSnd = computeVcAux(prog.snd, cond, invs, union(vars, getVars(prog.fst)))
      return computeVcAux(prog.fst, vcSnd,invs, vars);
    case IFTE:
      //Here we hace some support of the semantics of the boolean interpretation.
      var vrs = union(vars, getVars(prog.cond));
      return ifte(
        prog.cond, 
        computeVcAux(prog.tcase, cond, invs, vrs), 
        computeVcAux(prog.fcase, cond, invs, vrs));
    case WHLE:
      //An very special case
      //We get all the variables of the condition, body of the cycle, variables modified, variables of the invariant, variables of the invariant in the body
      //Create de invariant body.
      var condVars = getVars(prog.cond);
      var bodyVars = getVars(prog.body);
      var modified = [...modifiedVars(prog.body)];
      var invVars = [...union(vars, union(condVars, bodyVars))].map(v => vr(v));
      var inv = invariant("inv" + invs, invVars);
      var vcBody = computeVcAux(prog.body, inv, invs+1, union(vars, condVars));
      var inv_p = invariant(inv.name, invVars);
      var cnd = Object.assign({}, prog.cond);
      //For every variable modified, we create a fresh variable.
      modified.forEach(v =>  {
        var modv = v + "__p"
        inv_p = subst(inv_p, v, vr(modv))
        cnd = subst(cnd, v, vr(modv))
        vcBody = subst(vcBody, v, vr(modv))
        cond = subst(cond, v, vr(modv))
      });
      return seq(inv, ifte(inv_p,
        ifte(cnd, vcBody, cond),
        tru()
      ));
  }
}

/**
 * Function that compute the Verification Condition.
 * @param {Exp} - Program that be analized.
 * @return{Exp} - Verification Condition.
*/
function computeVC(prog) {
    //Compute the verification condition for the program leaving some kind of place holder for loop invariants.
  // The input prog is an AST. The output is an AST representing the verification condition.
  return computeVcAux(prog, tru(), 0, new Set());
}

/*
 ************************************************************************************************************
 Verification Condition: Sketch
 ************************************************************************************************************
*/

/**
 * Function that verify if a predicate is true.
 * @param {Exp} - Predicate.
 * @return{Bool} - Satisfacibility of the predicate.
*/
function isTrue(e) {
  return e.type === NOT && e.left.type === FALSE;
}

/**
 * Function that construct the assert.
 * @param {Exp} - Program without asserts.
 * @return{Exp} - Program with asserts.
*/
function assertify(vc) {
  switch(vc.type) {
    case SEQ:
      var ass1 = assertify(vc.fst)
      var ass2 = assertify(vc.snd)
      return seq(ass1, ass2); break;
    case IFTE:
      var ass1 = assertify(vc.tcase)
      var ass2 = assertify(vc.fcase)
      return ifte(vc.cond, ass1, ass2);
    default:
      return assert(vc);
  }
}

/**
 * Function that get the invariants of the Verification Condition.
 * @param {Exp} - AST of the VC.
 * @return{Exp} - The assigment of the VC.
*/
function getInvs(vc) {
  switch(vc.type) {
    case INVAR:
      var invs = {}
      invs[vc.name] = vc.exprs.length
      return invs; break;
    case SEQ:
      return Object.assign({}, getInvs(vc.fst), getInvs(vc.snd)); break;
    case ASSERT:
      return getInvs(vc.exp); break;
    case IFTE:
      return Object.assign({}, getInvs(vc.cond), getInvs(vc.tcase), getInvs(vc.fcase)); break;
    default: return {}; break;
  }
}

/**
 * Function that create new variables.
 * @param {[Var]} - List of actual variables.
 * @return{[Var]} - Fresh variables.
*/
function generateVars(noVars) {
  var x = "x"
  var vars = []
  for(i = 0; i < noVars; i++) {
    vars.push(x + i)
  }
  return vars;
}

/**
 * Function that construct the invariant expression.
 * @param {[Var]} - List of variables.
 * @return{Exp} - Invariant expression.
*/
function generateInvs(invs) {
  var code = []
  for (var key in invs) {
    var vars = generateVars(invs[key])
    var str = "bit " + key + "("+ vars.map(v => "int " + v).join(', ') + ") {\n";
    str = str + "  return exprBool({"+ vars.join(', ') +"}, {PLUS});\n}";
    code.push(str);
  }
  return code;
}

/**
 * Function that cosntruct the VC for Sketch program.
 * @param {Exp} - AST of the VC.
 * @return{Exp} - Sketch code.
*/
function genSketch(vc) {
  //The input is the VC that was generated by computeVC. The output is a String 
  //representing the sketch that you can feed to the sketch solver to synthesize the invariants.
  var ass = assertify(vc)
  var vars = getVars(ass);
  var invs = getInvs(ass);
  var invCode = generateInvs(invs);


  var sketch = "harness void vc(" + [...vars]
    .map(v => "int " + v)
    .join(", ") + ") {\n" + ass.toString() + "\n}"
  var lines = sketch.split(/\r?\n/)
  var sketchIndent = []
  var indent = 0;

  lines.forEach(l => {
    if(l.startsWith("}"))
      indent -= 1
    sketchIndent.push('  '.repeat(indent) + l)
    if(l.endsWith("{"))
      indent += 1
  })

  return invCode.join('\n\n') + '\n\n' +  sketchIndent.join('\n');
}

/*
 ************************************************************************************************************
 Verification Condition: Test cases
 ************************************************************************************************************
*/

function prueba1(){
  var prog1 = seq(
    seq(
      seq(
        assume(le(num(0),vr("n"))),
        assume(le(num(0),vr("m"))),
        ),
      seq(
        seq(
          assgn("i",num(1)),
          assgn("r",vr("n"))
          ),
          whle(
            le(vr("i"),vr("m")),
            seq(
              assgn("r",plus(vr("r"),num(1))),
              assgn("i",plus(vr("i"),num(1))))
            )
          )
      ),
    //assert(and(lt(vr("m"),vr("i")),eq(vr("r"),plus(vr("n"),vr("m"))))));
    assert(eq(vr("r"), plus(vr("n"), vr("m")))));
  return prog1;
}

function test1(){
  var pr1 = prueba1();
  clearConsole();
  writeToConsole("Test 1:");
  writeToConsole(pr1.toString());
  writeToConsole("\nAST:");
  writeToConsole(computeVC(pr1).toString());
  writeToConsole("\nSketch:");
  writeToConsole(genSketch(computeVC(pr1)).toString());
}

function prueba2(){
  var prog2 = seq(
    seq(
      seq(
        assume(le(num(0),vr("n"))),
        assume(le(num(0),vr("m"))),
        ),
      seq(
        seq(
          assgn("i", num(0)),
          assgn("r", vr("n"))
          ),
          whle(
            le(vr("i"),vr("m")),
            seq(
              assgn("r", plus(vr("r"),vr("r"))),
              assgn("i", plus(vr("i"),num(1))))
            )
          )
      ),
    assert(and(le(vr("i"),vr("m")),le(vr("r"),times(vr("n"),vr("m"))))));
  return prog2;
}

function test2(){
  var pr2 = prueba2();
  clearConsole();
  writeToConsole("Test2:");
  writeToConsole(pr2.toString());
  writeToConsole("\nAST:");
  writeToConsole(computeVC(pr2).toString());
  writeToConsole("\nSketch:");
  writeToConsole(genSketch(computeVC(pr2)).toString());
}

function prueba3() {
  var prog3 = block([
    assume(eq(vr("x"), vr("x_0"))),
    assume(eq(vr("y"), vr("y_0"))),
    assume(lt(vr("x"), vr("y"))),
    assgn("t", plus(vr("y"), times(num(-1), vr("x")))),
    whle(lt(num(0), vr("t")), block([
      assgn("x", plus(vr("x"), num(1))),
      assgn("y", plus(vr("y"), num(-1))),
      assgn("t", plus(vr("t"), num(-1)))
    ])),
    assert(and(eq(vr("x"), vr("y_0")), eq(vr("y"), vr("x_0"))))
  ])
  return prog3;
}

function test3(){
  var pr3 = prueba3();
  clearConsole();
  writeToConsole("Test3:");
  writeToConsole(pr3.toString());
  writeToConsole("\nAST:");
  writeToConsole(computeVC(pr3).toString());
  writeToConsole("\nSketch:");
  writeToConsole(genSketch(computeVC(pr3)).toString());
}

function prueba4() {
  var prog4 = block([
    assume(eq(vr("x"), vr("y"))),
    assgn("t", num(0)),
    whle(lt(vr("t"), num(10)), block([
      ifte(lt(vr("t"), num(7)), block([
        assgn("x", plus(vr("x"), num(-1))),
        assgn("y", plus(vr("y"), num(-1)))
      ]), skip()),
      assgn("t", plus(vr("t"), num(1)))
    ])),
    assert(eq(vr("x"), vr("y")))
  ])
  return prog4;
}

function test4(){
  var pr4 = prueba4();
  clearConsole();
  writeToConsole("Test4:");
  writeToConsole(pr4.toString());
  writeToConsole("\nAST:");
  writeToConsole(computeVC(pr4).toString());
  writeToConsole("\nSketch:");
  writeToConsole(genSketch(computeVC(pr4)).toString());
}

function prueba5() {
  var prog5 = block([
    assume(lt(vr("x"), vr("y"))),
    assume(eq(times(vr("x"), vr("t")), vr("y"))),
    assgn("i", num(0)),
    whle(lt(vr("i"), vr("t")), block([
      assgn("i", plus(vr("i"), num(1)))
    ])),
    assert(eq(times(vr("x"), vr("i")), vr("y")))

  ])
  return prog5;
}

function test5(){
  var pr5 = prueba5();
  clearConsole();
  writeToConsole("Test5:");
  writeToConsole(pr5.toString());
  writeToConsole("\nAST:");
  writeToConsole(computeVC(pr5).toString());
  writeToConsole("\nSketch:");
  writeToConsole(genSketch(computeVC(pr5)).toString());
}

function P2a() {
    var prog = eval(document.getElementById("p2input").value);
    clearConsole();
    writeToConsole("Just pretty printing for now");
    writeToConsole(computeVC(prog).toString());
}

function P2b() {
    var prog = eval(document.getElementById("p2input").value);
    clearConsole();
    writeToConsole("Just pretty printing for now");
    writeToConsole(genSketch(computeVC(prog)).toString());
}


//Some functions you may find useful:
function randInt(lb, ub) {
    var rf = Math.random();
    rf = rf * (ub - lb) + lb;
    return Math.floor(rf);
}

function randElem(from) {
    return from[randInt(0, from.length)];
}

function writeToConsole(text) {
    var csl = document.getElementById("console");
    if (typeof text == "string") {
        csl.textContent += text + "\n";
    } else {
        csl.textContent += text.toString() + "\n";
    }
}

function clearConsole() {
    var csl = document.getElementById("console");
    csl.textContent = "";
}
